﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RQKG2_Tarea_1__hola_mundo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnuno_Click(object sender, EventArgs e)
        {
            label1.Text = "Hola mundo de PCP 1-1";
        }

        private void btndos_Click(object sender, EventArgs e)
        {
            label1.Text = "Adiós mundo de PCP 1-1";
        }
    }
}
